#!/usr/bin/env python3


def cat_search():

	print("""  ,-.       _,---._ __  / \ """)
	print(""" /  )    .-'       `./ /   \  """)
	print("""(  (   ,'            `/    /|""")
	print(""" \  `-"             \ \   / |""")
	print("""  `.              ,  \ \ /  |""")
	print("""   /`.          ,'-`----Y   |""")
	print("""  (            ;   cat  |   '""")
	print("""  |  ,-.    ,-' search  |  /""")
	print("""  |  | (   |   for IP   | /""")
	print("""  )  |  \  `.___________|/""")
	print("""  `--'   `--" """"")

def cat_done():
	print("""  /\      /|""")
	print(""" {  `---'  }""")
	print(""" {  O   O  }""")
	print(""""~~>  V  <~~""")
	print("""  \  \|/  /""")
	print("""   `-----'____""")
	print("""   /     \    \_""")
	print("""  {       }\  )_\_   _""")
	print("""  |  \_/  |/ /  \_\_/ )""")
	print("""   \__/  /(_/     \__/""")
	print("""    (__/""")
	print("All Done!")
